/**start DARK MODE */
const btn = document.querySelector(".btn-toggle");
const theme = document.querySelector("#theme-link");

// Lắng nghe sự kiện click vào button
btn.addEventListener("click", function () {
  // Nếu URL đang là "main.css"
  if (theme.getAttribute("href") == "./css/main.css") {
    // thì chuyển nó sang "dark-theme.css"
    theme.href = "./css/dark-theme.css";
  } else {
    // và ngược lại
    theme.href = "./css/main.css";
  }
});

/** start BACK TO TOP BUTTON */
// Get the button:
let mybutton = document.getElementById("myBtn");
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
  scrollFunction();
};
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
/** end BACK TO TOP BUTTON */
